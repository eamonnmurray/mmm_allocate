mmm_allocate.py
===============

This is a python script to work out the division of hours for Imperial
departmental projects on the [Tier 2 MMM Hub:
Thomas](https://wiki.rc.ucl.ac.uk/wiki/Thomas).

Imperial gets an allocation of 3996077.00 hours every 3 months. We divide this
out in the following way:

- Hours for the first month are divided evenly between departments. (Period 0).
- Hours for the second and third month are divided according to the previous
  months usage. (Period 1 and 2).
- In the last two weeks all remaining hours are pooled and redistributed
  according to the last four weeks of usage. (Period 3).

This script doesn't run any commands on the cluster, but instead will generate
a set of `gtransfer` commands that can be copy and pasted (after confirming
they're correct) depending which period we're in.

This script doesn't keep any history of allocations, instead interacting with
the scripts that access Gold accounting on the machine. Some of these commands
are a little slow, but this keeps things simpler, and ensures it will still
work as normal if the filesystem is slow.

We rely on three commands:

- `gbalance` - this gives a list of the total, reserved and available hours for
  each project. We use the output of this to determine the likely period, and
  to count how many hours are reserved by each project. These are weighted by
  half when determining total usage.
- `glsalloc` - this is used to find the dates the current allocation(s) are
  valid.
- `gstatement` - this accesses the Gold database and can get a list of all job
  charges for a project. These charges are used to determine total usage for
  each project, along with the amount of reserved hours from `gbalance`, with
  the latter contribution divided by 2.

The list of accounting commands that may be useful is available at the
[Points of Contact wiki page](https://wiki.rc.ucl.ac.uk/wiki/Points_of_Contact).

Usage
-----

The script can be run directly, and takes no arguments. **It does not perform
any credit transfers on the machine**, instead only outputing a number of
groups of transfer commands that can be used to make different types of
distributions.

It will output the following information:

- The beginning and end date of any current allocations. There is usually a
  week of overlap between the beginning of a new allocation and the end of the
  previous one, so two sets of dates may be listed. Otherwise allocations are
  active for three months.
- The remaining balance in the central allocation. This account receives
  Imperial's full allocation every three months, for further distribution by
  the point of contact.
    - The remaining balance is also used to find which division period we will
      next enter, and to suggest which set of gtransfer commands should be run.
- The usage for each project in the last 30 days. In tallying the total usage,
  the total charges as returned by `gstatement` for that project are added,
  along with the amount of reserved hours weighted by half. *Note: it takes a
  few seconds to run `gstatement` for each project.*
- A set of `gtransfer` commands that can be copy and pasted in a terminal to do
  each of three different kinds of division:
    1. An equal distribution, for the start of a new allocation (Period 0).
    2. A distribution weighted by total usage in the past 30 days (Periods 1
    and 2).
    3. A redistribution of all unused hours in all projects (Period 3). This
    last redistribution is optional, but has been useful in using up as much of
    our allocation as possible. Typically this would be done two weeks before
    the allocations expire.
