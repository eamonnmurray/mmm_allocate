#!/usr/bin/env python3

# Python script to work out division of hours for Imperial departmental
# projects on the Tier 2 MMM Hub: Thomas.

# Imperial gets an allocation of 3996077.00 hours every 3 months.
# We divide this out in the following way:
# Hours for the first month are divided evenly between departments.
# Hours for the second and third month are divided according to the previous
# months usage.
# In the last two weeks all remaining hours are pooled and redistributed
# according to the last four weeks of usage.

# This script doesn't run any commands on the cluster, but instead will
# generate a set of gtransfer commands that can be copy and pasted (after
# confirming they're correct) depending which period we're in.

# This script doesn't keep any history of allocations, instead interacting
# with the scripts that access Gold accounting on the machine. Some of these
# commands are a little slow, but this keeps things simpler.

# We rely on three commands:
# gbalance - this gives a list of the total, reserved and available hours
#     for each project. We use the output of this to determine the likely
#     period, and to count how many hours are reserved by each project. These
#     are weighted by half when determining total usage.
# glsalloc - this is used to find the dates the current allocation(s) are
#     valid.
# gstatement - this accesses the Gold database and can get a list of all
#     job charges for a project. These charges are used to determine total
#     usage for each project.

import datetime
import subprocess
from collections import OrderedDict

# Gold commands:
GBALANCE = "gbalance"
GLSALLOC = "glsalloc"
GSTATEMENT = "gstatement"

# These should never need to change, but if they do adjust the number of
# hours we get per quarter, or the account it goes to, these should be changed.
CENTRAL_PROJ = "Imperial_allocation"
CENTRAL_ALLOC = 3996077.00

def get_period(central_balance):
    """Get the current period based on the remaining main allocation."""
    frac = central_balance / CENTRAL_ALLOC
    if frac > 0.9:
        return 0
    elif frac > 0.6:
        return 1
    elif frac > 0.3:
        return 2
    else:
        return 3

def get_alloc_dates():
    """Find all currently valid allocation dates.

    Parse the output of glsalloc to find when whatever current allocation
    periods are valid start and finish.
    """
    allocs = subprocess.check_output([GLSALLOC + " | grep " + CENTRAL_PROJ +
        " | grep -v infinity"], shell=True)
    date = datetime.datetime.today().strftime('%Y-%m-%d')
    # Filter out the allocations that have already expired, or have not yet
    # started. Since the dates returned by glsalloc are in YYYY-MM-DD we
    # can use string comparison directly.
    # The one complication here is that new allocations typically overlap the
    # old ones by around a week, so there may be two valid allocations
    # depending on when this is run.
    current_allocs = []
    for line in allocs.splitlines():
        eles = str(line).split()
        startdate = eles[3]
        enddate = eles[4]
        if (date > startdate and date < enddate):
            current_allocs.append(eles[3:5])
    return current_allocs

def get_usage(reserved, ndays=30, verbose=False):
    """Find the hours used for each project for the past ndays days."""
    usage = OrderedDict()
    period_start = datetime.datetime.today() - datetime.timedelta(days=ndays)
    period_start = period_start.strftime('%Y-%m-%d')
    tot_usage = 0.0
    # Add up the charges as this is the most direct thing to do, then add
    # the reserved amount weighted by half.
    if verbose:
        print("Getting usage for the past %d days." % ndays)
    for proj in reserved:
        usage[proj] = float(subprocess.check_output([GSTATEMENT + " -p " +
            proj + " -s " + period_start + " --summarize | awk 'BEGIN{s=0} "
            "/Charge/{s+=$6} END{printf \"%.2f\\n\", -s}'"],
            shell=True)) + reserved[proj]
        tot_usage += usage[proj]
        if verbose:
            print("%15s: %10.2f hours" % (proj, usage[proj]))

    # Convert this to relative usage, normalized to 1.
    for proj in reserved:
        usage[proj] /= tot_usage
    return usage

def parse_balances():
    """Find available and reserved balances from the gbalance command."""
    projs = []
    balances = subprocess.check_output([GBALANCE + " | grep Imperial_"],
            shell=True).splitlines()
    reserved = OrderedDict()
    avail = OrderedDict()

    for line in balances:
        ele = str(line).split()
        if ele[1] == CENTRAL_PROJ:
            central_balance = float(ele[2])
        else:
            projs.append(ele[1])
            reserved[ele[1]] = float(ele[3])
            avail[ele[1]] = float(ele[4])
    return central_balance, reserved, avail

def gen_evendistrib(projects, hours):
    """Generate the commands for an even distribution of hours."""
    even_hours = hours / len(projects)
    for proj in projects:
        print("gtransfer --fromProject " + CENTRAL_PROJ + " --toProject " +
                proj + " {:0.2f}".format(even_hours))

def gen_wdistrib(usage, hours):
    """Generate the commands to distribute hours according to usage."""
    for proj, frac in usage.items():
        print("gtransfer --fromProject " + CENTRAL_PROJ + " --toProject " +
                proj + " {:0.2f}".format(hours * frac))

def gen_redist(avail, usage):
    """Generate the commands to pool and redistribute hours by usage."""
    tot_avail = 0.0
    for proj, hours in avail.items():
        tot_avail += hours
    deltas = {}
    for proj, frac in usage.items():
        deltas[proj] = tot_avail * frac - avail[proj]
    # Generate the output in increasing order of delta.
    # Negative deltas are transferred to the central account before
    # positive deltas are transferred out.
    for proj, delta in sorted(deltas.items(), key=lambda x:x[1]):
        if delta < 0.0:
            print("gtransfer --fromProject " + proj + " --toProject " +
                    CENTRAL_PROJ + " {:0.2f}".format(-delta))
        else:
            print("gtransfer --fromProject " + CENTRAL_PROJ + " --toProject "
                    + proj + " {:0.2f}".format(delta))

def main():

    central_balance, reserved, avail = parse_balances()
    print(datetime.date.today())
    alloc_dates = get_alloc_dates()
    for dates in alloc_dates:
        print("There is a current allocation from %s to %s." % tuple(dates))
    print("The central allocation has %.2f out of %.2f hours remaining."
            % (central_balance, CENTRAL_ALLOC))
    period = get_period(central_balance)
    # Subtract 2 when calculating hours per month to prevent overrunning the
    # central allocation due to numerical inaccuracy.
    month_hrs = (CENTRAL_ALLOC - 2) / 3.0
    print("This indicates we will next enter period %d of our allocation." %
            period)
    alloc_ops = {0: "Even Distribution", 1: "Weighted Distribution",
            2: "Weighted Distribution", 3: "Redistribution"}
    print("The likely desired operation is a " + alloc_ops[period] + ".")
    print()

    usage = get_usage(reserved, ndays=30, verbose=True)
    print()

    print("Copy and paste the desired set of gtransfer commands below:\n")

    print("Even Distribution")
    gen_evendistrib(reserved.keys(), month_hrs)
    print()

    print("Weighted Distribution")
    gen_wdistrib(usage, month_hrs)
    print()

    print("Redistribution")
    gen_redist(avail, usage)
    print()

if __name__ == '__main__':
    main()
